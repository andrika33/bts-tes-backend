<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\RegisterController;
use App\Http\Controllers\Api\LoginController;
use App\Http\Controllers\Api\CheckListController;
use App\Http\Controllers\Api\ChecklistItemController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/



Route::post('/register', [RegisterController::class, '__invoke']);
Route::post('/login', [LoginController::class, 'authenticate']);

Route::group(['middleware' => ['jwt.verify']], function() {
    Route::get('/checklist', [CheckListController::class, 'index']);
    Route::post('/checklist', [CheckListController::class, 'store']);
    Route::delete('/checklist/{checklistId}',  [CheckListController::class, 'destroy']);
    //checklist items
    Route::get('/checklist/{checklistId}/item', [ChecklistItemController::class, 'index']);
    Route::post('/checklist', [ChecklistItemController::class, 'store']);
    Route::delete('/checklist/{checklistId}',  [ChecklistItemController::class, 'destroy']);
});
