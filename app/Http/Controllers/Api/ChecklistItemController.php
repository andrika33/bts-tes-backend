<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Validator;

class ChecklistItemController extends Controller
{
    public function index($checklistId)
    {
        $data = DB::table('checklist_items')->where('checklistId', $checklistId)->get();

        if($data) {
            return response()->json([
                'code' => '00',
                'message' => 'success',
                'data'    => $data,  
            ], 200);
        }

        return response()->json([
            'code' => '00',
            'message' => 'failed',
        ], 409);
    }

    public function store(Request $request)
    {
        //Validate data
        $validator = Validator::make($data, [
            'itemName' => 'required|string',
            'checklistId' => '',
        ]);

        //Send failed response if request is not valid
        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages()], 200);
        }

        
        $data = DB::table('checklist_items')->insert([
            'itemName' => $request->itemName,
            'checklistId' => $request->checklistId,
        ]);

        //Product created, return success response
        return response()->json([
            'code' => '00',
            'message' => 'checklist created successfully',
            'data' => $data
        ], Response::HTTP_OK);
    }
}
