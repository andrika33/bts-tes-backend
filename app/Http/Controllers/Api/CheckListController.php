<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Validator;

class CheckListController extends Controller
{
    protected $user;
 
    public function __construct()
    {
        $this->user = JWTAuth::parseToken()->authenticate();
    }

    public function index()
    {
        $data = DB::table('checklist')->get();

        if($data) {
            return response()->json([
                'code' => '00',
                'message' => 'success',
                'data'    => $data,  
            ], 200);
        }

        return response()->json([
            'code' => '00',
            'message' => 'failed',
        ], 409);
    }

    public function store(Request $request)
    {
        //Validate data
        $data = $request->only('name');
        $validator = Validator::make($data, [
            'name' => 'required|string',
        ]);

        //Send failed response if request is not valid
        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages()], 200);
        }

        
        $data = DB::table('checklist')->insert([
            'name' => $request->name,
        ]);

        //Product created, return success response
        return response()->json([
            'code' => '00',
            'message' => 'checklist created successfully',
            'data' => $data
        ], Response::HTTP_OK);
    }

    public function destroy($checklistId)
    {
        $data = DB::table('checklist')->where('id', $checklistId)->delete();
        
        return response()->json([
            'code' => '00',
            'message' => 'checklist deleted successfully'
        ], Response::HTTP_OK);
    }
}
