<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## Installasi 
- git clone https://gitlab.com/andrika33/bts-tes-backend.git
- copy .env.sample change to .env
- composer install
- composer dump-autoload
- php artisan key:generate
- php artisan jwt:secret
- php artisan migrate

## document api

- Registrasi
```
method post
uri: http://127.0.0.1:8000/api/register
body: {
     "username" : "user",
     "email" : "user@mail.com",
     "password": "1234567"
}
```

- Login
```
method post
uri: http://127.0.0.1:8000/api/login
body: {
     "username" : "user",
     "password": "1234567"
}
```

- Checklist
```
method get
uri: http://127.0.0.1:8000/api/checklist

method post
uri: http://127.0.0.1:8000/api/checklist
body : {
  "name": "string"
}

method delete
uri: http://127.0.0.1:8000/api/checklist/{checklistId}
```

- Checklist item
```
method get
uri: http://127.0.0.1:8000/api/checklist/{checklistId}/item
```
